module gitlab.com/universe-libraries/golang-google/storage

go 1.12

require (
	cloud.google.com/go v0.41.0
	google.golang.org/api v0.7.0
)
